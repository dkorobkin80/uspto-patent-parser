"""
Класс для работы с базой данных

(с) Алейников А.А.

14.05.2022
"""
from clickhouse_driver import Client
from dictionary import *

class DatabaseClient:

    """
    Подключение к БД и формирование таблиц
    """
    def __init__(self):
        # Подключение к БД

        self.client = Client('localhost')

        try:
            self.client.execute("create database if not exists %s" % (database_name))
        except Exception as error:
            print("Ошибка подключения к базе данных: " + str(error))
            exit(0)
        # Создание таблицы с патентами
        self.client.execute("drop table if exists %s" % (patents_table))
        self.client.execute(
            "create table %s (id int, invention_title String, publication_reference String, date String, "
            "abstract String, assignee_id int ) engine = MergeTree() order by id;" % (patents_table))

        # Создание таблицы патенты-заявители
        self.client.execute("drop table if exists %s" % (patent_applicant_table))
        self.client.execute(
            "create table %s (id int, patent_id int, applicant_id int) engine = MergeTree() order by id;" % (patent_applicant_table))

        # Создание таблицы с заявителями
        self.client.execute("drop table if exists %s"  % (applicants_table))
        self.client.execute("create table %s (id int, fullname String) engine = MergeTree() order by "
                            "id;" % (applicants_table))

        # Создание таблицы с владельцами прав
        self.client.execute("drop table if exists %s" % (assignee_table))
        self.client.execute("create table %s (id int, company_name String) engine = MergeTree() order by "
                            "id;" % (assignee_table))
        self.client.execute("insert into %s values (1, 'None')" % (assignee_table))

        # Создание таблицы с патентными классами ipcr
        self.client.execute("drop table if exists %s" % (class_table))
        self.client.execute("create table %s (id int, classification String) engine = MergeTree() order by "
                            "id;" % (class_table))

        # Создание таблицы патенты-классы
        self.client.execute("drop table if exists %s" % (patent_class_table))
        self.client.execute(
            "create table %s (id int, patent_id int, classification_id int) engine = MergeTree() order by id;" % (
                patent_class_table))

        # Создание таблицы с цитатами
        self.client.execute("drop table if exists %s" % (citations_table))
        self.client.execute("create table %s (id int, citation_number String) engine = MergeTree() order by "
                            "id;" % (citations_table))

        # Создание таблицы патенты-цитаты
        self.client.execute("drop table if exists %s" % (patent_citation_table))
        self.client.execute(
            "create table %s (id int, patent_id int, citation_id int) engine = MergeTree() order by id;" % (
                patent_citation_table))

        # Создание таблицы описание в hadoop
        self.client.execute("drop table if exists %s" % (description_table))
        self.client.execute(
            "create table %s (id int, description String) ENGINE=HDFS('hdfs://namenode:9000/user/root/description/d*.csv', 'CSV');" % (
                description_table))

        # Создание таблицы описание в hadoop
        self.client.execute("drop table if exists %s" % (claim_table))
        self.client.execute(
            "create table %s (id int, claim String) ENGINE=HDFS('hdfs://namenode:9000/user/root/claim/c*.csv', 'CSV');" % (
                claim_table))


    """
    Метод возвращает клиент базы данных
    """
    def createClient(self):
        return self.client
