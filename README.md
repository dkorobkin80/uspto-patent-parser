# Модуль для анализа патентного массива и формирования обучающих выборок
## Назначение разработки
Разрабатываемая программа предназначена формирования обучающих выборок из патентного массива. Программное средство осуществляет парсинг патентов USPTO, сохраняет полученные текстовые поля и метаинформацию в файловую систему HDFS и СУБД Clickhouse соответственно.
## Установка
Для работы модуля необходимы Clickhouse и HDFS, развернутые в Docker контейнерах.

`docker run --restart always -d -p 18123:8123 -p 9000:9000 --name clickhouse-volume --ulimit nofile=262144:262144 --volume=$HOME/click:/var/lib/clickhouse yandex/clickhouse-server `

`docker network create cluster`

`docker network connect cluster clickhouse-volume`

`git clone https://github.com/big-data-europe/docker-hadoop`

`cd docker-hadoop`

Заменить файл docker-compose.yml на файл из данного репозитория.

`docker-compose up -d`

## Использование

- Скачать файлы с сайта USPTO из раздела Patent Grant Full Text Data (No Images) https://bulkdata.uspto.gov

- Распаковать архивы в папку

- Указать абсолютный путь до папки в файле dictionary.py в переменную `uspto_dir`
