"""
Точка входа приложения

(с) Алейников А.А.

14.05.2022
"""
import time

from parser import PatentParser


if __name__ == '__main__':
    startTime = time.time()

    parser = PatentParser()
    parser.parse()

    endTime = time.time()
    print("Время обработки: " + str(round(((endTime - startTime) / 60), 2)) + " мин")
